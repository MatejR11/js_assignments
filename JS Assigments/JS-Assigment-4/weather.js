const axios = require('axios');

class WeatherInfo {
  constructor(city) {
    this.city = city;
  }

  async getForecast() {
    let city = this.city;

    try {
      const response = await axios.get(`https://api.weatherapi.com/v1/current.json?key=00b3369b045d4a65b9a72633210408&q=${city}&aqi=no`);
      console.log(response.data)
    } catch (err) {
      console.log(err);
    }
  }
}

let cities = new WeatherInfo("Rijeka");
cities.getForecast();
