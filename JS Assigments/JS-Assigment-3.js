function factorial(num) {
  if (num === 0) {
    return 1;
  } else {
    return num * factorial(num - 1);
  }
}

// console.log(factorial(5));

let userInfo = {
  name: 'Matej',
  birthdate: new Date(2001, 2, 29),
  gender: 'male',
  hasAPet: true,
  numberOfCountriesVisited: 8,
};

let cloneOne = userInfo;

let cloneNamedDifferent = { ...cloneOne, name: 'Matej Ruzic' };

console.log('\n');
console.log(
  userInfo === cloneOne,
  ' - ',
  'This is TRUE because cloneOne has a reference to userInfo'
);

let address = {
  address: 'Bencani 23',
};

userInfo = { ...userInfo, ...address };
cloneNamedDifferent = { ...cloneNamedDifferent, ...address };

console.log('\n');
console.log(
  userInfo.address === cloneNamedDifferent.address,
  ' - ',
  'This is also TRUE since the ADDRESS property of both objects contains the same value'
);

function deleteField(objectArray, field) {
  let newArr = objectArray.map(({ [field]: omit, ...rest }) => rest);

  return newArr;
}

let arr = [userInfo, cloneOne, cloneNamedDifferent];

// console.log('\n');
// console.log(deleteField(arr, 'address'));

function Rectangle(a, b) {
  this.a = a;
  this.b = b;
}

let rect = new Rectangle(4, 5);

rect.length = rect.a;
rect.width = rect.b;
rect.perimeter = (rect.a + rect.b) * 2;
rect.area = rect.a * rect.b;

function getMaxAndMin(arr) {
  var smallest = Infinity;
  var largest = -Infinity;

  arr.forEach((element) => {
    if (largest < element) {
      largest = element;
    }

    if (smallest > element) {
      smallest = element;
    }
  });

  let minMax = {
    min: smallest,
    max: largest,
  };

  return minMax;
}

// console.log('\n');
// console.log(getMaxAndMin([8]));
// console.log(getMaxAndMin([-1345, 1, 564, 17896]));
// console.log(getMaxAndMin([]));
// console.log(getMaxAndMin([Infinity, -Infinity]));
