function getGrade(score) {
  let grade;

  if (score >= 0 && score <= 30) {
    grade = 'F';
  } else if (score > 31 && score <= 45) {
    grade = 'D';
  } else if (score > 45 && score <= 65) {
    grade = 'C';
  } else if (score > 65 && score <= 84) {
    grade = 'B';
  } else if (score > 84 && score <= 100) {
    grade = 'A';
  }

  return grade;
}

function getAverageAge(ageArray) {
  let average = 0;
  let sum = 0;

  ageArray.forEach((element) => {
    sum += element;
  });

  average = sum / ageArray.length;

  return average;
}

function getDayName(dayNumber) {
  switch (dayNumber) {
    case 1: {
      return 'Monday';
    }

    case 2: {
      return 'Tuesday';
    }

    case 3: {
      return 'Wednesday';
    }

    case 4: {
      return 'Thursday';
    }

    case 5: {
      return 'Friday';
    }

    case 6: {
      return 'It is weekend';
    }

    case 7: {
      return 'It is weekend';
    }

    default: {
      return 'Invalid day number';
    }
  }
}

console.log(getGrade(86));
console.log(getGrade(40));

console.log(getAverageAge([12, 55, 64, 34, 90, 53, 29]));
console.log(getAverageAge([4, 6, 29, 68, 44]));

console.log(getDayName(3));
console.log(getDayName(6));
console.log(getDayName(19));
