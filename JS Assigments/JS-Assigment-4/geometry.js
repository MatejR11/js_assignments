let pi = 3.14159;

class Shape {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  x = this.x;
  y = this.y;

  perimeter() {
    return undefined;
  }

  area() {
    return undefined;
  }
}

class Rectangle extends Shape {
  perimeter() {
    return 2 * this.x + 2 * this.y;
  }

  area() {
    return this.x * this.y;
  }
}

class Square extends Shape {
  perimeter() {
    return 4* this.x;
  }

  area() {
    return this.x * this.x;
  }
}

class Circle extends Shape {
  constructor(x, y, r) {
    super(x, y);
    this.r = r;
  }

  perimeter() {
    return 2 * this.r * pi;
  }

  area() {
    return this.r * this.r * pi;
  }

}

let rect = new Rectangle(4, 5);
let sqr = new Square(4, null);
let circ = new Circle(null, null, 5);

console.log(`Perimeter of rectangle is ${rect.perimeter()}.`);
console.log(`Area of rectangle is ${rect.area()}.`);
console.log("\n");
console.log(`Perimeter of square is ${sqr.perimeter()}.`);
console.log(`Area of square is ${sqr.area()}.`);
console.log("\n");
console.log(`Perimeter of circle is ${circ.perimeter()}.`);
console.log(`Area of circle is ${circ.area()}.`);
