let mutableString = '';
const immutableString = 'Immutable string';

let mutableNumber = 3.141592653589;
const immutableNumber = 11;

let mutableBoolean = true;
const immutableBoolean = false;

let nullVar = null;
const undefinedVar = undefined;

const arr = [
  mutableString,
  immutableString,
  mutableNumber,
  immutableNumber,
  mutableBoolean,
  immutableBoolean,
  nullVar,
  undefinedVar,
];

for (let i = 1; i <= 5; i++) {
  let section = console.log(
    `[----------------- SECTION ${i} -----------------]`
  );

  switch (i) {
    case 1: {
      section;

      arr.forEach((element, index) => {
        console.log(index, ' - ', element);
      });

      console.log('\n');
      break;
    }

    case 2: {
      section;

      arr.forEach((element) => {
        console.log(typeof element);
      });

      console.log('\n');
      break;
    }

    case 3: {
      section;

      const resultOfSum = mutableNumber + immutableNumber,
        resultOfSubtraction = mutableNumber - immutableNumber,
        resultOfMultiplication = mutableNumber * immutableNumber,
        resultOfDivision = mutableNumber / immutableNumber;

      console.log(
        '',
        { sum: resultOfSum },
        '\n',
        { subtraction: resultOfSubtraction },
        '\n',
        { multiplication: resultOfMultiplication },
        '\n',
        { division: resultOfDivision }
      );

      console.log('\n');
      break;
    }

    case 4: {
      section;

      mutableString = 'Galatasaray';

      console.log(`${mutableString} ${immutableString}`);

      console.log('\n');
      break;
    }

    case 5: {
      section;

      console.log(
        mutableNumber.toString(),
        ' --- ',
        immutableNumber.toString(),
        '\n'
      );
      console.log('', { normalCheck: nullVar == undefinedVar }, '\n', {
        strictCheck: null === undefinedVar,
      });

      console.log('\n');
      break;
    }
  }
}
